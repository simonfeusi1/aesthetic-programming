***MiniX1 - ReadMe - 07/02/21 - Simon Feusi*** 


![](Skærmbillede_2021-02-07_kl._21.34.52.png)




What have I produced? 
- I have produced two different objects with a neon-green background ('rgb(0,255,0)'). The left object is a rotating quadratic box in a 3D-format that changes its colour nuances while rotating (purple, pink, red, blue, grey, black ). The other object is a circle and it shares the same nuances as the prior object. The circle is only shown in a 2D-format. 
The code is an example from the reference list found on p5.js' reference page (https://p5js.org/reference/). I only changed a few numbers in the codes to get a sense of how it would affect my illustration in the live-server. 

How would I describe my first independent coding experience? 
- My first idependent coding experience in p5.js was challenging but educative. Sometimes I would face a wall and get frustrated because I wouldn't know how to continue the code. It takes surplus and motivation to keep going and I would get that from taking a break and drink some coffee.
Whenever I succeeded with somehting it felt awesome and it was a huge motivational boost! Im already excited to work with miniX2 and challenge myself more. 

How is the coding process different from, or similar to, reading and writing text?
- It is different in a way that is does not make sense for me in the same way. This is quite obvious since I've learned to read and write at a young age. They are similiar in a way that they both work as languages, just in a different way. When coding, you use a library full of functions to create something. When writing a text you do the same. You use the library of letters to create a product = word/sentence. 

What does code and programming mean to me, and how does the assigned
reading help me to further reflect on these terms?
- Coding and programming means that I get to explore a whole new world that I know very little about. It means that I can gain a better understanding of coding in JavaScript, more specifically in p5.js, and create aesthetically pleasing objects. The assigned reading helps me to further reflect on these terms because the reading show different aspects of the topic. Moreover, I think it is interesting and very relevant how the founder of p5.js puts a focus on including minorities in an otherwise very white-male-dominant industry. I didn't think of it as an issue in this industry before I was introduced to it, in the first class. 


[RunMe link](https://simonfeusi1.gitlab.io/aesthetic-programming/miniX1) 

**References**
Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48

p5.js. p5.js | get started. Available at: https://p5js.org/get-started/ [Accessed 09 Sept 2019]

Video: Lauren McCarthy, Learning While making P5.JS, OPENVIS Conference (2015).

Video: Daniel Shiffman, Code! Programming with p5.js, The Coding Train. Available at https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 [Accessed 09 Sep. 2019]. (watch 1.1)

Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on blackbord under the Literature folder)








           
