**MiniX2** - **14/02/2021** - **ReadMe** - **RunMe**

created by Simon Feusi 

![](Skærmbillede_2021-02-16_kl._10.48.34.png)
![](Skærmbillede_2021-02-16_kl._10.52.49.png)

**Describe your program and what you have used and learnt.**

--> The program consists of two main objects, which are the face in the top-left of the canvas, and "ghost" pacman in the bottom-right. The face shows different emotions with its worried eyes and shut/closed mouth. The ghost pac-man disappears, but then, reappears whenever you slide your mouse over the canvas. If you want the pac-man to dissappear and reappear very fast, you can manually slow down the framerate by moving the mouse slower and vice versa. I added the moving bar in order to give the program a playful effect. Also, the bar changes between random colors, which also gives it a hectic vibe. I kept the colors rather simple since it wasn't my focus in this program, compared to my miniX1. Moreover, there is no special meaning with the color choice, except to make the program more attractive. I rather wanted to play with the moving bar and see how its movement would create some kind of action/reaction. whenever the bar would pass the face from left to right, the face will opens its mouth and look schocked, since the pac-man would dissapper and reappear.
Since I am still in a learning process where i don't feel familiar with many syntaxes and their use yet, I kept my program rather basic. I was also behind on time and didn't use my ful potential on this miniX. But I did learn on how to work with the whichbar-syntax and what elements I shoud include to get a colorful one. I only used three different shapes (rec, ellipse, arc) in order to create my objects. When I look back on my program I would have liked to give my face and pac-man more personality by adding shapes and contrast. 


**How would you put your emoji into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on?**

--> I think it is very essential to take note of the importance of emojis as a communication tool. That is applicable for a casual setting as well as a professional setting. Emojis are in our everyday lives and they have begun to gain more and more meaning with time. In many cases the emoji took over the word itself and can easily be expressed by this small pixelled illustration. 
I gave my emoji (the face) an expression that could change -> so it would turn from a relaxed to a schocked face. Emojis primarily only show one expression, because there always will be another emoji with the second expression you might be looking for. But I thought it would be interesting if you could combine emojis and their emotions/expressions, so that they would fit perfect into a specific context. This would be interesting to use because it would create unique expressions that people might have, and it would thereby create a more realistic picture of the person you are texting with's emotions. 


[RunMe link](https://simonfeusi1.gitlab.io/aesthetic-programming/miniX2) 


**References**

Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70

Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016 

p5.js. p5.js | Simple Shapes. [Web] Available at: https://p5js.org/examples/hello-p5-simple-shapes.html

Daniel Shiffman, Code! Programming with p5.js, The Coding Train [online] Available at: https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2. [1.3,1.4,2.1,2.2]


