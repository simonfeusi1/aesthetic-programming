// Moving bar adjustments 
const barWidth = 20;
let lastBar = -1;


function setup() {
  createCanvas(1200, 1200);
  }

//Drawing of the top-left face
function draw() {
  background(3);
  fill(200)
  text(mouseX + "," + mouseY, 20, 20)
  fill(255);
  ellipse(300,200,300,300);
  let c=color(0,126,255)
  fill(c);
  ellipse(300,200,300,300)
  let value=(c);
  fill(value);
  rect(180,135,70,70);
  fill(0);
  rect(180,135,70,70);
  fill(255);
  rect(350,135,70,70);
  circle(220,180,20);
  fill(0);
  circle(390,180,20)
   let whichBar = mouseX / barWidth;
  if (whichBar !== lastBar) {
    let barX = whichBar * barWidth;
    fill(mouseY, height, height);
    rect(barX, 0, barWidth, height);
    lastBar = whichBar;
    text(mouseX + "," + mouseY, 20, 20)
  fill(255);

    //creating the ghost pac-man
  colorMode(HSB)
    fill(255,204,100);
  arc(600,500,400,400, -90,-10);
  let c=color(0,126,255)
  fill(c);
  ellipse(300,200,300,300)
  let value=(c);
  fill(value);
  rect(180,135,70,70);
  fill(0);
  rect(180,135,70,70);
  fill(255);
  rect(350,135,70,70);
  circle(220,180,20);
  fill(0);
  circle(390,180,20)
  circle(693,424,35)
  }

  //Change of mouth color and size.
  //Makes the ghost pac-man dissapear and reappear
  if (mouseX > 150 && mouseY > 50) {
  fill('brown');
  rect(240,220,120,80);
} else {
  fill('white');
  rect(350,250,-100,10);}
  }
