**MiniX5 - A Generative Program - 14/03/2021**

[click here to see the program](https://simonfeusi1.gitlab.io/aesthetic-programming/miniX5/)

![](Skærmbillede_2021-03-14_kl._19.51.52.png)



The creation of my generative program is based on two simple rules, which results in an (almost) endless creation of random ellipses with different locations and radiuses.  

_PROGRAM RULES_
1. draw a random ellipse followed by another random ellipse.(variation in radius and location)
2. Create adjacent ellipses without them overlapping. 

My program starts off with creating several ellipses in random locations. These ellipses can create a radius between 0.5 and 50 and therefore they variate in size. The ellipses keep producing more ellipses and the canvas will after a short period of time create a whole network with different sizes of ellipses that are connected. When running the program, it will show bigger ellipses in the beginning, but since the canvas only has the size of the window's width and height it will create smaller and smaller ones that can fit in between the ones that dominate in size. This will make it difficult to spot the development of the ellipses as they get "microscopic". 

In relation to the term 'emergent behaviour' that was introduced to us in our required readings, emergent behaviour is achieved by setting a few simple rules. These rules create the generativce feature in the program as it keeps repeating the commando, in this case, the ellipses with the radius shown visually. The emergent behaviour plays an important role in this program since the rules change the overall appearance. I wanted to keep it simple and thereby only use the blue, yellow, and faded orange colour. The program might have been more interesting to observe if various colours were added.
Emergent behaviour is shown through the repitition that keeps going on and on, without the creator having to do anything, except entering the code. This generative proces is out of our hands and is dependant on the algorithmic procedures. This can also be seen in other contexts, where the human cognitive activity has been reduced to algorithmic procedures. A given example is seen in the book from Soon & Cox where this could have consequences for political decision- making (p. 137 - example form Berardi). 

**Note:** 
My miniX5 has drawn inspiration from a program found on 'Generative Gestaltung – Creative Coding im Web', found in ISBN: 978-3-87439-902-9, First Edition, Hermann Schmidt, Mainz, 2018. 
I already had a thought about producing a program that was close to the one I was drawing inspiration from. This program helped me a lot to gain a better understanding of a generative program and how the different syntaxes and functions where used. I studied them for quite a long time and it was a very learning experience. I'm sure I will use them in some of my future miniX'es. 


**References** 

- Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
- 'Generative Gestaltung – Creative Coding im Web', found in ISBN: 978-3-87439-902-9, First Edition, Hermann Schmidt, Mainz, 2018. 

