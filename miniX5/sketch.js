

var circles = [];

var minRadius = 0.5;
var maxRadius = 50;

// for mouse and up/down-arrow interaction
var mouseRect = 15;

//var freeze = false;

var showCircle = true;
var showLine = true;

function setup() {
  createCanvas(windowWidth, windowHeight);
  noFill();
  ellipseMode(RADIUS);
}

function draw() {
  background(0,171,255);

  // Choosing a random or the current mouse position
  var newX = random(maxRadius, width - maxRadius);
  var newY = random(maxRadius, height - maxRadius);
  if (mouseIsPressed == RIGHT) {
    newX = random(mouseX - mouseRect, mouseX + mouseRect);
    newY = random(mouseY - mouseRect, mouseY + mouseRect);
  }

  // Try to fit the largest possible circle at the current location without overlapping
  var intersection = false
  for (var newR = maxRadius; newR >= minRadius; newR--) {
    for (var i = 0; i < circles.length; i++) {
      var d = dist(newX, newY, circles[i].x, circles[i].y);
      intersection = d < circles[i].r + newR;
      if (intersection) {
        break;
      }
    }
    if (!intersection) {
      circles.push(new Circle(newX, newY, newR));
      break;
    }
  }

  for (var i = 0; i < circles.length; i++) {
    if (showLine) {
      // Try to find an adjacent circle to the current one and draw a connecting line between the two
      var closestCircle;
      for (var j = 0; j < circles.length; j++) {
        var d = dist(circles[i].x, circles[i].y, circles[j].x, circles[j].y);
        if (d <= circles[i].r + circles[j].r + 1) {
          closestCircle = circles[j];
          break;
        }
      }
      if (closestCircle) {

        //defining color and size of radius lines.
        stroke(255, 230, 0);
        strokeWeight(2);
        line(circles[i].x, circles[i].y, closestCircle.x, closestCircle.y);
      }
    }

    // Drawing of the circles.
    if (showCircle) circles[i].draw();
  }
}

function Circle(x, y, r) {
  this.x = x;
  this.y = y;
  this.r = r;

  Circle.prototype.draw = function() {
    stroke(180);
    strokeWeight(2);
    ellipse(this.x, this.y, this.r);
  };

  // press keys '1' or '2' to only show lines or circles.
  if (key == '1') showCircle = !showCircle;
  if (key == '2') showLine = !showLine;
}
