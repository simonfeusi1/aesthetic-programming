#### MiniX9 - Que(e)ry data - Created by Mathilde, Sofie, and Simon 

![](knap.png)
![](memes.png)


### Link for our [MiniX9](https://mathildebg.gitlab.io/aesthetic-programming/miniX9/)
Our RunMe is on the link above
 
### Link for our repository - [the code](https://gitlab.com/simonfeusi1/aesthetic-programming/-/blob/master/miniX9%20/sketch.js)
Our repository/ the code for the program is on the link above

**What is the program about? Which API have you used and why?**

Our program is exploring the “raw” data behind the API and how the data is used to categorize gifs. We have used the GIPHY API, as a way of exploring a different representation of data. We see GIPHY as a site that often is associated with underlying and hidden values as Google often is. GIF’s can sometimes be seen as innocent and funny and you often forget how all this data is being categorized and can in many ways be seen as a reflection of the society and culture we live in. We chose this API to get a closer look on how the data is collected, stored, recommended, selected and most importantly how different queries reveal racial issues within the data. 
Our program picks random gifs from the GIPHY API. We query this data with two search words: “White people'' and “Black people'' and it is from these queries that the gif’s is being randomly picked from. When the mouse is being pressed a random set of gifs are being displayed on the screen randomly from the two categories. The gifs are being mixed and intertwined with each other. Even though it might seem difficult to distinguish the two categories from each other it is quite easy to see which gif originates from where, primarily based on the skin colour. 
We decided to create a button that says “click for meme”, in order to make a meme appear everytime you click the button. You won’t know from which query the following meme will come from since it picks a random popular meme from one of the queries. We wanted to set it up in that way in order to let the user guess if it is a “white meme” or “black meme”. In terms of letting the users guess it themselves, we also wanted to focus on stereotypes that are portrayed in these memes. There is often a clear distinction between them, one meme being “black” and another one being “white”. These distinctions are often seen when it comes to stereotypical phrases that might sound “black” or “white”, which is based on how society stereotypically sees black vs white people talking/writing. 

**Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data?**

For our project we chose to use GIPHY. In comparison to a few other API’s we’ve examined it was quite simple to access the data that GIPHY provides. You’re also allowed to very easily change different parameters in your data. For us that meant choosing search words for our GIFs and thereby choosing which topic we wanted to discuss with our work. Accessing the data in our sketch was also fairly simple, and the only issue we had this week was with the code itself. 


Representing the data collected is difficult to do with no prejudice. There will always be a certain bias in our work, since we ourselves chose the search words for the miniX9, which were “White People” and “Black People”. Therefore the representation is also closely linked to our choice of words. 

**How much do you understand this data or what do you want to know more about?**

Even in this work where we chose the API and data we wanted to use ourselves, there’s still so much information in the JSON files, that we don’t take advantage of in our RunMe. We very explicitly chose the data we wanted to present and neglected the other information on the file. This is both because of our need to narrow the work down to something understandable, but also because there still is a lot of data we don’t quite know how to process. 

**How do platform providers sort the data and give you the requested data?** 

This quote taken from the book, talks about the issues platform providers face when sorting the data. 
“It is important to recognize how all techniques of pattern recognition and statistics “generate statements and prompt actions in relation to instances of individual desire” and they transform, construct, and impose shape on data, in order to then “discover, decide, classify, rank, cluster, recommend, label or predict” something or other.” (Soon & Cox, 206) 
Sorting data into categorizations can be seen as an argument of keeping adding to the existence of classes and classifications. This is definitely one of the main problems we see within the API’s. This is why we see the importance of querying data and to understand how this is affecting and shaping the reality we live in. 

**What is the significance of APIs in digital culture?**

APIs are significant in digital culture since it is an easy method to let your product or service communicate with other products or services - it’s a software intermediary that allows two applications to speak to each other.
Furthermore, API’s are saving a lot of working hours for many people since digital processes and datasets easily can be packaged into modules, which can be re-used and recombined for different applications. An article posted by the EU (European Commission) called Igniting the digital transformation of governments with APIs show a row of examples across Europe where API’s have been the gateway for efficiency in different systems. One example that they mention is Estonia’s X-Road platform that got off the ground with an initial financing of a mere 300 000 euros. It links up public and private sector data, so that citizens only have to share data with the government once, saving an estimated 800 working years every single week.
We think that this example really shows the efficiency of API’s. We might oversee them because they are so integrated in today’s digital culture, but they are saving us more time than we could ever imagine. 


 
**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**

In relation to the topics we’ve discussed this week, we’ve found it interesting to talk about why certain data about us are being kept away from us. Especially why big tech companies such as Facebook and Twitter, but also governments are choosing to hide their API’s. This led us to discuss what this information includes and if they’re hiding it for our own security, or more likely, to withhold greater power over us?




**References**

- Soon Winnie & Cox, Geoff, "Que(e)ry data", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 186-210

- ec.europa.eu. (2020, 17 sept). EU SCIENCE HUB - The European Commission's science and knowledge service. Igniting the digital transformation of     governments with APIs. [online] Available at:
https://ec.europa.eu/jrc/en/news/igniting-digital-transformation-governments-apis  [Accessed 18 April 2021].
