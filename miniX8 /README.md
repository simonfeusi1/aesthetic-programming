# READ ME for our MiniX8
----
## MiniX8 for Aesthetic Programming at Aarhus University
 
![](screenshot.png)
 
 
 
### Link for our [MiniX8](https://mathildebg.gitlab.io/aesthetic-programming/miniX8/)
Our RunMe is on the link above
 
### Link for our repository - [the code](https://gitlab.com/mathildebg/aesthetic-programming/-/blob/master/miniX8/sketch8.js)
Our repository/ the code for the program is on the link above
 
----
 
### README
 
#### Provide a title of your work and a short description (1,000 characters or less)
Just Another News Story… this is a generative piece, inspired by this http://torquetorque.net/crashblossoms/#/home. Our program generates random news titles that most of the time make little to no sense. The titles are totally random and at times pretty humorous.
 
 
#### Describe how your program works, and what syntax you have used, and learnt?
Our program is generative and keeps combining random words,  but from specific word classes, into sentences. These are supposed to look like news titles in a newspaper. We use the random-function that can choose between three different word classes (arrays) that are called 0,1,2. The arrays are the following word classes: adjectives, subjects, and verbs. The array function with adjectives is used twice since they appear in the beginning and at the end of the sentences.
Furthermore, we preloaded our data which is defined as a variable and is equivalent to the JSON file where we have our word classes. We also renamed our function setup/draw in order to incorporate vocable code that is relevant for the context of our program. We changed the frameRate to 0.3 in order to give the viewers a chance to read the different news articles that are generated.
 
 
#### Analyze your own e-lit work by using the text Vocable Code and/orThe Aesthetics of Generative Code (or other texts that address code/voice/language)
As we were more focused on getting the code to work, and figuring out how best to work together we didn’t focus much on turning our own program into e-lit work. So there really isn’t much to analyze. Though we did notice how making little tweeks in the naming of files could make the code more intertwined with the actual program.
 
 
#### How would you reflect on your work in terms of Vocable Code?
It was more of an afterthought because we were so focused on implementing the JSON file correctly and what our runMe would look like, so we didn’t consider it as much as we initially wanted, but we feel that from now on it will be easier to consider vocable code when making a program because we do find it very fascinating and the concept of vocable code is super interesting and something we will have in mind. But in our code as it was an afterthought we did change the names of our functions we would normally just name function setup{} and function draw {}, we changed these to reflect the meaning of our program, so by reading the code you would get some sort of sense of our reason for making our program.
 
 
#### References
- Torquetorque.net. 2021. IF & ONLY IF. [online] Available at: http://torquetorque.net/crashblossoms/#/home [Accessed 11 April 2021].
 
- Soon, W., Cox, G. (2020). Aesthetic Programming - a Handbook on Software Studies. Open Humanities Press.
http://openhumanitiespress.org/books/download/Soon-Cox_2020_Aesthetic-Programming.pdf
 
- Amiraha.com. (2021). mexicans in canada. [online] Available at: http://amiraha.com/mexicansincanada/[Accessed 11 April 2021].
 
- Vectors, R. and Vectors, N. (2021). Blank newspaper template daily news folded paper vector image on VectorStock. [online] VectorStock. Available at: https://www.vectorstock.com/royalty-free-vector/blank-newspaper-template-daily-news-folded-paper-vector-27477730 [Accessed 11 April 2021].
