//Our global variables
var classifier;
var input; //Input box
var button_start; //Button
var button_submit; //Button
var button_done; //Button
var mode; // A variable for the modes
var data; //To preload our JSON file
var imageNumber = -1; // There is added one in the next screen function, so the first value is actually 0.
var imageArray = [];
var userImageArray = [];
var userTagArray = []; // The array is empty right now, but when the user presses the button, the tag will get into the array.
var inputCount = 0; // How many inputs we get from the user, this will determine when other tags will be shown

function preload() {
  //The data set we use
  classifier = ml5.imageClassifier('MobileNet');
  //The text font we use
  myFont = loadFont('textType.ttf');

  //We load the data from our JSON file
  data = loadJSON("tags.JSON");

  //Bagground picture for the front screen
  frontPage = loadImage('frontPage.png');

  //Different buttons
  leavePageButton = loadImage('leavePageButton.png');
  nextImageButton = loadImage('nextImage.png');
  submitTagButton = loadImage('submitTag.png')
}

function setup() {
  mode = 0; //Sets the mode for setup as 0
  button_start = createImg('start_button.png');

  //A for loop that runs through all of our pictures
  for (let i = 0; i < data.tags.length; i++) {
    imageArray.push(loadImage(data.tags[i].imageFile));
    userImageArray.push(loadImage(data.tags[i].userImageFile));
  }
}

function showUserTag() {
  userTagArray.push(input.value());
  var tagText = userTagArray.join("\n"); //New line after each of the users input
  textFont(myFont);
  textSize(26);
  fill(0, 0, 250);
  text(tagText, 1095, 125);
  inputCount++;

  if (inputCount > 2) {
    showHumanTags();
    showMachineTags();
  }
}

function endScreen() {
  mode = 2; //This function is also set as mode 2
  background(0, 0, 250); // A background is drawn in this mode
  //we use remove to remove DOM elements
  button_submit.remove();
  button_done.remove();
  button_next.remove();
  input.remove();
}

function draw() {
  if (mode == 0) { // Yhis determines what happens in mode 0
    createCanvas(1430, 710);
    background(frontPage);
    button_start.position(1209, 366);
    button_start.mousePressed(startPressed);
  }

  function startPressed() {
    mode = 1; //When button_start is pressed the mode changes from 0 to 1
    nextScreen();
    button_submit = createImg('submitTag.png');
    button_done = createImg("leavePageButton.png");
    button_next = createImg('nextImage.png');
    input = createInput();
    button_start.remove();
    //Informing text for the user to understand what to do
    textFont(myFont);
    fill('blue');
    textSize(20);
    text('Hello!\nYour job today is to describe\nfollowing pictures with 3 tags \nthank you for your cooperation', 20, 150);
  }

  if (mode == 1) { //Everything in here defines what happens in mode 1

    button_submit.position(600, 640);
    button_submit.mousePressed(showUserTag);
    input.position(595, 605);
    input.size(200, 20);
    input.style("border-color", "black");

    //The done button, if the user wants to finish the program berfore making tags for all the pictures, this button leads to the endscreen
    button_done.position(20, 30);
    button_done.mousePressed(endScreen);

    // The next picture button
    button_next.position(1260, 670);
    button_next.style("width", "155px");
    button_next.mousePressed(nextScreen); // The button leads to the next screen
  }

  if (mode == 2) { //everything in here defines what happens in mode 2

    background(0, 0, 250);
    //We use remove to remove DOM elements
    button_submit.remove();
    button_done.remove();
    button_next.remove();
    input.remove();

    // Endscreen "The blue screen of death"

    strokeWeight(2);
    fill(255);
    textSize(16);
    textAlign(LEFT);
    text("[noERROR HAS OCCURED_END_SCREEN]\n\n If this is the first time you have seen this error screen, you have finished our program.\n if this screen appears again you must really enjoy this program.. \n \n Follow these steps:\n Check to make sure you understand why this program was made, if this is clear ask yourself what the consequences of machine learning can be?\n \n If problems continue check your biases, and privileges. Disable ignorance and question everything. Remember nothing is neutral. \n \n \n Technical Information: \n \n *********STOP: [ERROR] THE ALGORITHM(); can not be defined.exe***** ACCOUNTABILITY.exe can’t be found404\n \n Though it can be hard to understand what is behind an algorithm\n and it is easier to simply excuse any faults on “the black box” it is important to understand\n the underlying human agency that is behind the design and the choices of data used.\n \n By participating in this program you have become responsible for how future algorithms label pictures[ERROR] ****LIE DETECTED**** \n \n Please contact Group 5 or a technical support group for more biased information", 200, 150);
    textAlign(CENTER);
  }
}

function nextScreen() {
  clear();
  imageNumber++;
  //An if statement to bring the user to the endscreen after making tags for all the pictures
  if (imageNumber === imageArray.length) {
    endScreen();
  }
  // This part of our code determines what  picture and which tags are being shown on canvas, it makes sure that the right tags are shown for the specific picture
  background(userImageArray[imageNumber]);
  // This is to reset the userTagArray, so the old tags will not follow
  userTagArray = [];
  inputCount = 0;

  textFont(myFont);
  fill("blue");
  textSize(20);
  text("Add your 3 tags here and unlock the rest -->", 150, 626);
}

function showHumanTags() {
  textFont(myFont);
  textSize(26);
  var label = data.tags[imageNumber].words[0];
  text(label, 890, 620);
}

function showMachineTags() {
  classifier.classify(imageArray[imageNumber], gotResult);
}

function gotResult(error, results) {
  // Display error in the console
  if (error) {
    console.error(error);
  } else {
    // The results are in an array ordered by confidence.
    var computerTags = [];
    var numberOfTags = 3;

    for (var i = 0; i < numberOfTags; i++) {
      computerTags.push(results[i].label);
    }
    fill(0, 0, 255);
    var tagText = computerTags.join("\n");
    text(tagText, 25, 440);
  }
}
