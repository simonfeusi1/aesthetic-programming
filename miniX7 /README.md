[click here to see the program](https://simonfeusi1.gitlab.io/aesthetic-programming/MiniX7/)
![](pong.png)


**My program**

I used Shifmann's "pong coding challenge", which creates the old school pong game. 
There is a puck that moves around the canvas. The puck is controlled by the paddles that are placed on the left and right side of the canvas and it moves a long the y-axes to protect each sides. When the puck passes the paddle, the opponent earns a point. The paddles are controlled with keyboard-buttons, in this case 'a' and 'z' for the left paddle and 'j' and 'm' for the right paddle. The puck always keeps the same pace. 


**Objects and their related attributes**

Since this is an object-oriented-programmed game, I defined classes for the paddels and the puck. I used the function "constructor" where I could define different variables that would design the sizes and location of each element. 
Moreover, Shifmann's coding challenge introduced me to "sin" and "cos" functions that where essential for the angles when the puck hits the paddles (ll. 22+23, 37+38, 53+54). I didn't think further about in the beginning, but it makes a lot of sense that the program needs to know in what direction/angle the puck is bouncing back in. 

**Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?**

Object-oriented programming can be seen as an abstraction of the real world. This object-oriented way of programming (OOP) really puts the social and technological context into perspective, on how objects relate to each other. Not only in programming, but also in our society.

"With the abstraction of relations, there is an interplay between abstract and concrete reality. The classifications abstract and concrete were taken up by Marx in his critique of capitalist working conditions to distinguish between abstract labor and living labor. Whereas abstract labor is labor-power exerted producing commodities that uphold capitalism, living labor is the capacity to work." (Cox & Soon, 161)

I think this quote describes OOP as an interplay between abstract and concrete reality very well. In many ways our world is complex and difficult to figure out, but when using OOP we try to minimize the complexity in some sort of way, at least visually. 
It is also quite interesting to think about how computers think and interpret data and how we as humans do it. The computer needs something specific and tangible before it can give an object properties, whereas we as humans define, for example a human, based on many more aspects. We need personality, feelings, thoughts and so on and so on... But still, we find OOP as some sort of bridge between the computer and us and a way and a method of abstracting on a computational level but also on a culture- and society-based level.

**NOTE**
My program suddenly had difficulties running and the console said that "Puck" was not defined. I struggled to find the issue since it has worked before...

**References**

- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Daniel Shiffman, “Code! Programming with p5.js,” The Coding Train (watch: 2.3, 6.1, 6.2,6.3, 7.1, 7.2, 7.3), https://www.youtube.com/watch?v=8j0UDiN7my4&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA.
- Daniel Shiffman, "Coding Challenge #67: Pong!", The Coding Train, https://www.youtube.com/watch?v=IIrC5Qcb2G4&t=1781s. 

