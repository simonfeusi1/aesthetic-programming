[click to see my program](https://simonfeusi1.gitlab.io/aesthetic-programming/miniX6/)

![](box_from_inside.png)

**My changes and why** 

In this miniX we were supposed to rework one of our old MiniX and I chose to rework my first one. 
I didn't do the big changes on my miniX but I wanted to create a program that showed the rotating box from the inside. The idea behind it was to somehow "create" another dimension and to bring another visual perspective on the previous program. When I created my first program that contained a rotating box and an ellipse, I didn't think how much new information I would learn about programming. I think it was quite difficult to bring an old program "back to life" and give it new changes. I wanted to show the progression in a metaphorical way where the box is portraying that. 
When looking at a RunMe from a miniX, or just a program in general, you don't necessarily think about all the functions and syntaxes (the code) behind it. I wanted to show that by showing the inside of the box. The box from the outside can show how we as users (and not as programmers) see a finished product/program. The inside, however, keeps all the data and codes that have been used to create this box that we are looking at. 

**Aesthetic Programming and Critical Making**

I think it was challenging to get a complete understanding of what "aesthetic programming" means but after working with critical making in our lectures it made me realise that this field is "build" upon a lot of aspects, such as _critical - site - reflection - politics and power dynamics - critical theory - imaginary - alternative - intervention_. I didn't put as much focus on these aspects in my ReadMe's as i wanted to, looking back on them now. I wanted to include more reflections about my work. My focus was primarily on the code itself and I invested a lot of time to go through them many times and to understand the complexity behind some of them. 
In terms of crtical making, I got an epiphany when reading the following quote: 
'Ultimately, critical making is about turning the relationship between technology and society from a "matter of fact" into a "matter of concern"'.(Ratto, Matt & Hertz, Garnet, 2019, "The Critical Makers Reader: (Un)Learning Technology", p. 20)
It seems very relevant in the way our mindset should be during this course. It is always important to reflect and question the unquestioned. Moreover, I think it is important to think in these ways when working with critical making too. Critical making can provide an understanding of the consequences and the impact that a product can have on its environment, as well as making you reflect about certain issues or topics that occur in the proces of making/creating. Some things might seem as a "matter of fact", but in order to break structures it is necessary to question and make it a concern. 

**NOTE**
My program works fine in ATOM live server but i have difficulties to make it work through the link. 


**References**

- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
- Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28 (available free online: https://networkcultures.org/blog/publication/the-critical-makers-reader-unlearning-technology/)
