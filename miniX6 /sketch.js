

function setup() {
  createCanvas(1000, 500, WEBGL);
}

function draw() {
 background;

  let locX=mouseX-height/2;
  let locY=mouseY-width/2;

  ambientLight(50);
  directionalLight(255,0,0,0.25,0.25,0);
  pointLight(0,0,255,locX,locY,250);


  push();
  translate(30,0,0);
  rotateZ(frameCount*0.02);
  rotateX(frameCount*0.02);
  specularMaterial(250);
  box(1000, 1000, 1000);
  pop();

}

print(mouseX, mouseY);

const barWidth = 20;
let lastBar = -1;
