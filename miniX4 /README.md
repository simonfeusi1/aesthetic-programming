### IRRELEVANT INFORMATION

**At the Transmediale, Berlin, 2021** 

![](screenshot1.PNG)


**Provide a title for and a short description of your work (1000 characters or less) as if you were going to submit it to the festival.**

My project "Irrelevant Information" is adressing how companies repeal irrelevant information from their users, but also how face-scanning can track down your information from various sources. Furthermore, we are familiar with data being sold to second and third parties without the user knowing about it, and these are the main points that my project wants to illuminate. 
I've created something that should match the looks of a sketchy pop-up homepage, with an advertisement for a new skin-treatment, called the "laser-tan". If the user gives access to the camera, red lines will track down your face and visualize how the treatment would look like in real life. Right beneath there are a lot of blank spaces where the user should fill out unnecessary information in order to submit it and book an appointment. BUT, as the text says right after, the company already received all the irrelevant information by scanning, and therey tracking, your face. In that case you don't have to fill out the blank spaces manually. You are already familiar to them!  
At the end, the program overexaggerates the use of the "submit information" button, which creates this sketchy homepage look and also looks like spam and virus. 

I've learned a lot about the facetracking functions and how they work in practice. I did use the functions that Winnie wrote down and then I tried to play with it. I must admit that I had a hard time adjusting the different information, especially for the button. I wanted to change the overall style, but I found it quite difficult to manage around in the code. 

In relation to the theme of "capture all", my project illuminates the amount of data that can be sold companies on homepages, apps, etc. Often, too much data is captured and it can eventually extend to an irrelavent category of data, where the information shared is not used for its correct purpose. It might be profitable though..... 

**NOTE**
My program works fine in ATOM live server but i have difficulties to make it work through the link. 


[click here to see RunMe](https://simonfeusi1.gitlab.io/aesthetic-programming/miniX4/)

**References** 

- Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
- Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.


