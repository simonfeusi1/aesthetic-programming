let button;
let mic;
let ctracker;
let capture;
let img;

function preload() {

  img = loadImage('GDPR.jpg');
}

function setup() {
  createCanvas(600, 900);
  background('rgb(100%,0%,10%)');

  // video capture
  capture = createCapture(VIDEO);
  capture.size(640, 480);
  capture.hide();

  // Audio capture
  mic = new p5.AudioIn();
  mic.start();

  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

  //styling the 'me' button with CSS. This is primarily the information from Winnie's example.
  button = createButton('you');
  button.style("display", "inline-block");
  button.style("color", "'0'");
  button.style("padding", "5px 8px");
  button.style("text-decoration", "none");
  button.style("font-size", "0.9em");
  button.style("font-weight", "normal");
  button.style("border-radius", "10px");
  button.style("border", "0");
  button.style("text-shadow", "0 -1px 0 rgba(0, 0, 0, .2)");
  button.style("background", "#4c69ba");
  button.style(
    "background","-moz-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","-webkit-gradient(linear, left top, left bottom, \
      color-stop(0%, #3b55a0))");
  button.style(
    "background","-webkit-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","-o-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","-ms-linear-gradient(top, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "background","linear-gradient(to bottom, #4c69ba 0%, #3b55a0 100%)");
  button.style(
    "filter","progid:DXImageTransform.Microsoft.gradient \
    ( startColorstr='#4c69ba', endColorstr='#3b55a0', GradientType=0 )");
  //mouse capture
  button.mouseOut(revertStyle);
  button.mousePressed(changeBG);

}

// change of background? (this function is weird)
function changeBG() {
  let val = random();
  background(val);

}


function draw() {

  background(255,204,0);
  rect(0,450,640,750);
  textSize(20);
  translate(0,100);
  text('↓ TRY OUR NEW LASER-TAN FOR YOUR FACE ↓',75,-65);
  text('because your beauty matters to us ♥️',130,-35);
  text('Name:_________________',0,510);
  text('Age:__________________',0,540);
  text('Gender:_______________',0,570);
  text('Sexuality:_______________',0,600);
  text('Race:_________________',0,630);
  text('Height:_______________',0,660);
  text('Weight:_______________',0,690);
  text('Nevermind! We already received your irrelevant information from',0,750);
  text('various sources while scanning your face. Thank you.',0,780);
  text('☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼', 0,720);
  textFont("Avenir");
  createButton('submit information');
  //createButton('submit information')
  //createP('Nevermind, we already received your information by tracking your face. Thank you.');
  fill(150);
   //getting the audio data i.e the overall volume (between 0 and 1.0)
  let vol = mic.getLevel();
  /*map the mic vol to the size of button,
  check map function: https://p5js.org/reference/#/p5/map */
  button.size(floor(map(vol, 0, 1, 40, 450)));

  //draw the captured video on a screen with the image filter
  image(capture, 0,0, 640, 480);
  filter(ERODE);

  let positions = ctracker.getCurrentPosition();
  //check the availability of web cam tracking
  if (positions.length) {
     //point 60 is the mouth area
    button.position(positions[60][0]-20, positions[60][1]);
    /*loop through all major points of a face
    (see: https://www.auduno.com/clmtrackr/docs/reference.html)*/
    for (let i = 0; i < positions.length; i++) {
       noStroke();
       //color with alpha value
       fill(map(positions[i][0], 0, width, 100, 255), 0, 0, 120);
       //draw ellipse at each position point
       quad(positions[i][0], positions[i][1], 5, 5, 30, 40, 50, 60);

//adding the meme to the canvas :)
image(img,400,480);



    }
  }
}

function change() {
  button.style("background", "#2d3f74");
  userStartAudio();
}
function revertStyle(){
  button.style("background", "#4c69ba");
}
//keyboard capture
function keyPressed() {
  //spacebar - check here: http://keycode.info/
  if (keyCode === 32) {
    button.style("transform", "rotate(180deg)");
  } else {   //for other keycode
    button.style("transform", "rotate(0deg)");
  }
}
