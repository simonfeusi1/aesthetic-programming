**MiniX3 - 21/02/2021 - ReadMe - RunMe**


![](screenshot2.PNG)
**RELOADING YEAR 2021 **


**Description of my program**

My throbber is expressing the frustration of entering the year of 2021 and still being confronted with the restrictions, due to COVID-19, on a daily basis. The frameRate is set to 15 and creates this hectic and chaotic feeling, while spinning around fireworks and repeating to reload the year of 2021. I also wanted to create a RunMe with several dimensions, so there would be more throbbers to look at. 

There are several dimensions of throbbers in my program, and it all starts in the center of the screen, which was created with the syntax "translate(width/2, height/2);", that I use for the first time. The first rubber is created from the middle center, where the R's are circling around the center. This creates a small throbber in yellow. The next throbber is created by small red triangles that I just added for the looks. It had some kind of fire-element that I found matching to the fireworks. It covers the three last letters in "RELOADING", since we actually aren't reloading 2021, it can ony be a wish and not an action. The fireworks that appear in the outer areas do also create a colorful throbber that symbolize New Years Eve and all the hopes we had for a year with a normal pase. 
The program contains the syntaxes  let cir = 360/num*(frameCount%num); AND rotate(radians(cir));, which create the circular movement of the throbber. It spins 360 degress, which is divided with the amount of objects that circulate. This will then be multiplied with the number's percentage of the framecount. rotate(radians(cir)); let the program run in a circular movement since it says "cir". 
Time is created in computation by adjusting the framerate. I think it makes a big difference if the program is spinning fast or slow, because it somehow can make you feel that something is loading either slow or fast. 
"Technology has been a tool through which humans create distance from natural cycles and design their own time experiences." -Hans Lammerant. When desiging a throbber, we create a loop that might differntiate from the real time that we follow in our part of the world. Time is a social construction that we collectively choose to believe in. But what if we actually redefined time? How would the world be then? 

I also do think that throbbers of different characters can communicate various things. Some have very soothing expressions while others look rather blunted and messy. It is definitely a choice that depends on what kind of aesthetic you wish to create. My personal favorite are the very thin and simple ones. They give me some kind of calmness and they are not as annoying to look at in case you have to wait a long time. 

[RunMe link](https://simonfeusi1.gitlab.io/aesthetic-programming/miniX3/)

(I had a few problems with showing some lines that I typed into my code, but they seem to appear in the RunMe. On the other hand, now my jpg-file isn't showing anymore....)


**References/Required readings** 

- Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
- Daniel Shiffman, Code! Programming with p5.js on YouTube, The Coding Train. (3.1, 3.2, 3.3, 3.4, 4.1, 4.2, 5.1, 5.2, 5.3, 7.1, 7.2)


