
var img;
function setup(){
    createCanvas(windowWidth, windowHeight);
    img=loadImage('fireworks.jpg');
    frameRate(5);
}

function draw(){
  background(60,80);
  drawElements(); //you need to define it.
  image(img,0,0,windowHeight);
}

function drawElements(){
  let num=18;
  translate(width/2, height/2);
  //this makes it possible to move your object to tove in the middle of the canvas
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255,255,0);
  quad(38, 31, 86, 20, 69, 63, 30, 76);
  stroke(200,250,0);
  line(0,0,0,width);
  stroke(800,55,0);
  line(222,222,222,height);
  line(55,38,54,height);
  line(150,43,56,height);
  line(322,322,322,height);
  line(422,422,422,height);
  line(522,522,522,height);
  line(622,622,622,height);
  line(722,722,722,height);
  line(822,822,822, height);
  line(922,922,922,height);

  textSize(35);
  text('RELOADING YEAR 2021', 0,0);

  push(); // Start a new drawing state
  translate(150, -20);
  strokeWeight(50);
  fill(204, 153, 0);
  quad(0,15, 33, 33,78,44);
  pop(); // Restore original state

}

function windowResized(){
  resizeCanvas(windowWidth, windowHeight)
}
